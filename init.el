(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(helm-completing-read-handlers-alist
   (quote
    ((find-tag . helm-completing-read-default-find-tag)
     (xref-find-definitions . helm-completing-read-default-find-tag)
     (xref-find-references . helm-completing-read-default-find-tag)
     (ggtags-find-tag-dwim . helm-completing-read-default-find-tag)
     (tmm-menubar)
     (find-file)
     (execute-extended-command)
     (dired-do-rename . helm-read-file-name-handler-1)
     (dired-do-copy . helm-read-file-name-handler-1)
     (dired-do-symlink . helm-read-file-name-handler-1)
     (dired-do-relsymlink . helm-read-file-name-handler-1)
     (dired-do-hardlink . helm-read-file-name-handler-1)
     (basic-save-buffer . helm-read-file-name-handler-1)
     (write-file . helm-read-file-name-handler-1)
     (write-region . helm-read-file-name-handler-1)
     (cider-connect-cljs)
     (cider-connect-clj)
     (cider-connect)
     (cider-connect-clj&cljs))))
 '(helm-completion-style (quote emacs))
 '(package-selected-packages
   (quote
    (multi-term aweshell spacemacs-theme rg helm-swoop git-timemachine git-gutter pdf-tools company-auctex drag-stuff ace-jump-mode ace-jump yascroll ace-window clj-refactor smart-jump dumbjump helm-descbinds auctex feature-mode goto-line-preview expand-region beacon undo-tree highlight-symbol multiple-cursors multi-cursors helm-company company org-bullets helm-projectile helm-git-grep helm-ls-git flycheck-clj-kondo paredit magit rainbow-delimiters projectile cider clojure-mode-extra-font-locking clojure-mode exec-path-from-shell gnu-elpa-keyring-update doom-modeline helm which-key try use-package)))
 '(spacemacs-theme-comment-bg nil)
 '(spacemacs-theme-comment-italic t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(doom-modeline-bar ((t (:background "DodgerBlue1"))))
 '(doom-modeline-buffer-file ((t (:foreground "DodgerBlue1" :weight bold))))
 '(doom-modeline-buffer-major-mode ((t (:foreground "DodgerBlue1" :weight bold))))
 '(doom-modeline-project-dir ((t (:foreground "#FF9C0C" :weight bold))))
 '(focus-unfocused ((t (:foreground "dim gray"))))
 '(git-gutter:modified ((t (:inherit default :foreground "dodger blue" :weight bold))))
 '(helm-match-item ((t (:background "DeepPink1" :foreground "white" :weight bold))))
 '(helm-selection ((t (:background "DodgerBlue1" :foreground "white" :weight bold))))
 '(helm-source-header ((t (:inherit bold :background "#c56ec3" :foreground "white"))))
 '(helm-swoop-target-word-face ((t (:background "DeepPink1" :foreground "white" :weight bold)))))

(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(setq package-archive-priorities
      '(("melpa-stable" . 10)
	("melpa"     . 5)))

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(org-babel-load-file (expand-file-name "~/.emacs.d/config.org"))
